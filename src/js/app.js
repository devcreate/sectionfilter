import './modules/sitePreloader';
import documentReady from './utils/documentReady';
import documentLoaded from './utils/documentLoaded';

import cssVars from './modules/cssVars';
import resize from './modules/resize';
import lazyload from './modules/lazyload';
import search from './modules/search';
import ourworksLabel from './modules/ourworksLabel';



documentReady(() => {
  cssVars.init();
  resize.init();
  lazyload.init();
});

documentLoaded(() => {

});
