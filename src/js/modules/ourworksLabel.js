function label() {
  let spanLabels = document.querySelectorAll('.s-ourworks_label');

  spanLabels.forEach(
    function getMatch(info) {
      let nameLabels = info.querySelector('.s-ourworks__item-img-label');
      let nameLabelsInner = nameLabels.innerHTML;

      if (nameLabelsInner === 'IndependentLiving') {
        info.classList.add('s-ourworks_label-darkAzure');
      }
      else if (nameLabelsInner === 'SupportAvailable') {
        info.classList.add('s-ourworks_label-orange');
      }
      else {
        info.classList.add('s-ourworks_label-othere');
      }
    }
  )
}
label();

