/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"app": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "js/";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([0,"vendors"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app.js":
/*!****************!*\
  !*** ./app.js ***!
  \****************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modules_sitePreloader__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modules/sitePreloader */ "./modules/sitePreloader.js");
/* harmony import */ var _utils_documentReady__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./utils/documentReady */ "./utils/documentReady.js");
/* harmony import */ var _utils_documentLoaded__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utils/documentLoaded */ "./utils/documentLoaded.js");
/* harmony import */ var _modules_cssVars__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modules/cssVars */ "./modules/cssVars/index.js");
/* harmony import */ var _modules_resize__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modules/resize */ "./modules/resize.js");
/* harmony import */ var _modules_lazyload__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modules/lazyload */ "./modules/lazyload.js");
/* harmony import */ var _modules_search__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modules/search */ "./modules/search.js");
/* harmony import */ var _modules_ourworksLabel__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./modules/ourworksLabel */ "./modules/ourworksLabel.js");








Object(_utils_documentReady__WEBPACK_IMPORTED_MODULE_1__["default"])(function () {
  _modules_cssVars__WEBPACK_IMPORTED_MODULE_3__["default"].init();
  _modules_resize__WEBPACK_IMPORTED_MODULE_4__["default"].init();
  _modules_lazyload__WEBPACK_IMPORTED_MODULE_5__["default"].init();
});
Object(_utils_documentLoaded__WEBPACK_IMPORTED_MODULE_2__["default"])(function () {});

/***/ }),

/***/ "./modules/cssVars/index.js":
/*!**********************************!*\
  !*** ./modules/cssVars/index.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _vh__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./vh */ "./modules/cssVars/vh.js");
/* harmony import */ var _scrollWidth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./scrollWidth */ "./modules/cssVars/scrollWidth.js");


/* harmony default export */ __webpack_exports__["default"] = ({
  init: function init() {
    _vh__WEBPACK_IMPORTED_MODULE_0__["default"].init();
    _scrollWidth__WEBPACK_IMPORTED_MODULE_1__["default"].init();
  }
});

/***/ }),

/***/ "./modules/cssVars/scrollWidth.js":
/*!****************************************!*\
  !*** ./modules/cssVars/scrollWidth.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils_getScrollWidth__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../utils/getScrollWidth */ "./utils/getScrollWidth.js");

/* harmony default export */ __webpack_exports__["default"] = ({
  init: function init() {
    this.calculate();
  },
  calculate: function calculate() {
    var scrollWidth = Object(_utils_getScrollWidth__WEBPACK_IMPORTED_MODULE_0__["default"])();
    document.documentElement.style.setProperty('--scroll-width', "".concat(scrollWidth, "px"));
  }
});

/***/ }),

/***/ "./modules/cssVars/vh.js":
/*!*******************************!*\
  !*** ./modules/cssVars/vh.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _dispatcher__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../dispatcher */ "./modules/dispatcher.js");
/* harmony import */ var _utils_isTouchDevice__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../utils/isTouchDevice */ "./utils/isTouchDevice.js");


/* harmony default export */ __webpack_exports__["default"] = ({
  init: function init() {
    this.calculate();
    this.handleResize();
  },
  calculate: function calculate() {
    var vh = document.documentElement.clientHeight * 0.01;
    document.documentElement.style.setProperty('--vh', "".concat(vh, "px"));
  },
  handleResize: function handleResize() {
    var _this = this;

    _dispatcher__WEBPACK_IMPORTED_MODULE_0__["default"].subscribe(function (_ref) {
      var type = _ref.type;
      var targetType = Object(_utils_isTouchDevice__WEBPACK_IMPORTED_MODULE_1__["default"])() ? 'resize:both' : 'resize:height';

      if (type === targetType) {
        _this.calculate();
      }
    });
  }
});

/***/ }),

/***/ "./modules/dispatcher.js":
/*!*******************************!*\
  !*** ./modules/dispatcher.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils_EventEmitter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/EventEmitter */ "./utils/EventEmitter.js");

/* harmony default export */ __webpack_exports__["default"] = (new _utils_EventEmitter__WEBPACK_IMPORTED_MODULE_0__["default"]());

/***/ }),

/***/ "./modules/lazyload.js":
/*!*****************************!*\
  !*** ./modules/lazyload.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "../../node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "../../node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator */ "../../node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "../../node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.from */ "../../node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_is_array__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.is-array */ "../../node_modules/core-js/modules/es.array.is-array.js");
/* harmony import */ var core_js_modules_es_array_is_array__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_is_array__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "../../node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "../../node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.date.to-string */ "../../node_modules/core-js/modules/es.date.to-string.js");
/* harmony import */ var core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.function.name */ "../../node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "../../node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "../../node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "../../node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "../../node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "../../node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var core_js_features_object_assign__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! core-js/features/object/assign */ "../../node_modules/core-js/features/object/assign.js");
/* harmony import */ var core_js_features_object_assign__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(core_js_features_object_assign__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var intersection_observer__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! intersection-observer */ "../../node_modules/intersection-observer/intersection-observer.js");
/* harmony import */ var intersection_observer__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(intersection_observer__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var lozad__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! lozad */ "../../node_modules/lozad/dist/lozad.min.js");
/* harmony import */ var lozad__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(lozad__WEBPACK_IMPORTED_MODULE_17__);
















function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }


 // polyfill


/* harmony default export */ __webpack_exports__["default"] = ({
  init: function init() {
    var root = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : document;
    var options = {
      rootMargin: "".concat(document.documentElement.clientHeight, "px 0px")
    };
    var pictures = root.querySelectorAll('.js-lazy-img:not([data-loaded])');
    var backgrounds = root.querySelectorAll('.js-lazy-bg:not([data-loaded])');

    if (pictures.length) {
      var pictureObserver = lozad__WEBPACK_IMPORTED_MODULE_17___default()(pictures, options);
      pictureObserver.observe();
    }

    if (backgrounds.length) {
      var backgroundObserver = lozad__WEBPACK_IMPORTED_MODULE_17___default()(backgrounds, options);
      backgroundObserver.observe();
    }

    this.lazyVideo(root);
  },
  lazyVideo: function lazyVideo() {
    var root = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : document;

    var lazyVideos = _toConsumableArray(root.querySelectorAll('video.js-lazy-video:not([data-loaded])'));

    var observerOptions = {
      rootMargin: "".concat(document.documentElement.clientHeight, "px 0px")
    };

    if ('IntersectionObserver' in window) {
      var lazyVideoObserver = new IntersectionObserver(function (entries) {
        entries.forEach(function (video) {
          if (video.isIntersecting) {
            // eslint-disable-next-line no-restricted-syntax,guard-for-in
            for (var source in video.target.children) {
              // eslint-disable-next-line prefer-destructuring
              var videoSource = video.target.children[source];

              if (typeof videoSource.tagName === 'string' && videoSource.tagName === 'SOURCE') {
                // eslint-disable-next-line prefer-destructuring
                videoSource.src = videoSource.dataset.src;
              }
            }

            video.target.load();
            video.target.setAttribute('data-loaded', 'true');
            lazyVideoObserver.unobserve(video.target);
          }
        });
      }, observerOptions);
      lazyVideos.forEach(function (lazyVideo) {
        lazyVideoObserver.observe(lazyVideo);
      });
    }
  }
});

/***/ }),

/***/ "./modules/ourworksLabel.js":
/*!**********************************!*\
  !*** ./modules/ourworksLabel.js ***!
  \**********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "../../node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "../../node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_1__);



function label() {
  var spanLabels = document.querySelectorAll('.s-ourworks_label');
  spanLabels.forEach(function getMatch(info) {
    var nameLabels = info.querySelector('.s-ourworks__item-img-label');
    var nameLabelsInner = nameLabels.innerHTML;

    if (nameLabelsInner === 'IndependentLiving') {
      info.classList.add('s-ourworks_label-darkAzure');
    } else if (nameLabelsInner === 'SupportAvailable') {
      info.classList.add('s-ourworks_label-orange');
    } else {
      info.classList.add('s-ourworks_label-othere');
    }
  });
}

label();

/***/ }),

/***/ "./modules/resize.js":
/*!***************************!*\
  !*** ./modules/resize.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _dispatcher__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dispatcher */ "./modules/dispatcher.js");
/* harmony import */ var _utils_throttle__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/throttle */ "./utils/throttle.js");


/* harmony default export */ __webpack_exports__["default"] = ({
  size: {
    width: window.innerWidth,
    height: window.innerHeight
  },
  init: function init() {
    var self = this;
    window.addEventListener('resize', Object(_utils_throttle__WEBPACK_IMPORTED_MODULE_1__["default"])(300, false, function () {
      self.handleResize();
    }), false);
    window.addEventListener('orientationchange', Object(_utils_throttle__WEBPACK_IMPORTED_MODULE_1__["default"])(300, false, function () {
      self.handleResize();
    }), false);
  },
  handleResize: function handleResize() {
    /* eslint-disable prefer-destructuring */
    var width = window.innerWidth;
    var height = window.innerHeight;
    /* eslint-enable prefer-destructuring */

    var widthChanged = width !== this.size.width;
    var heightChanged = height !== this.size.height;

    if (widthChanged) {
      _dispatcher__WEBPACK_IMPORTED_MODULE_0__["default"].dispatch({
        type: 'resize:width'
      });
    }

    if (heightChanged) {
      _dispatcher__WEBPACK_IMPORTED_MODULE_0__["default"].dispatch({
        type: 'resize:height'
      });
    }

    if (widthChanged && heightChanged) {
      _dispatcher__WEBPACK_IMPORTED_MODULE_0__["default"].dispatch({
        type: 'resize:both'
      });
    }

    _dispatcher__WEBPACK_IMPORTED_MODULE_0__["default"].dispatch({
      type: 'resize:default'
    });
    this.size = {
      width: width,
      height: height
    };
  }
});

/***/ }),

/***/ "./modules/search.js":
/*!***************************!*\
  !*** ./modules/search.js ***!
  \***************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "../../node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_includes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.includes */ "../../node_modules/core-js/modules/es.array.includes.js");
/* harmony import */ var core_js_modules_es_array_includes__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_includes__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_string_includes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.string.includes */ "../../node_modules/core-js/modules/es.string.includes.js");
/* harmony import */ var core_js_modules_es_string_includes__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_includes__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "../../node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_3__);





function filter(evt) {
  evt.preventDefault();
  var input = document.querySelector('#s-ourworks__filters-input');
  var inputValue = input.value.toUpperCase();
  var cards = document.querySelectorAll('.s-ourworks__item');

  if (inputValue.length > 3) {
    cards.forEach(function getMatch(info) {
      var heading = info.querySelector('h4');
      var headingContent = heading.innerHTML.toUpperCase();

      if (headingContent.includes(inputValue)) {
        info.classList.add('show');
        info.classList.remove('hide');
      } else {
        info.classList.add('hide');
        info.classList.remove('show');
      }
    });
  } else {
    cards.forEach(function getMatch(info) {
      var heading = info.querySelector('h4');
      var headingContent = heading.innerHTML.toUpperCase();

      if (headingContent.includes(inputValue)) {
        info.classList.remove('hide');
        info.classList.add('show');
      } else {
        info.classList.remove('hide');
        info.classList.add('show');
      }
    });
  }
}

var form = document.querySelector('.s-ourworks__filters-form');
form.addEventListener('keyup', filter);

/***/ }),

/***/ "./modules/sitePreloader.js":
/*!**********************************!*\
  !*** ./modules/sitePreloader.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.symbol */ "../../node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.symbol.description */ "../../node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator */ "../../node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.concat */ "../../node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "../../node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.from */ "../../node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_array_is_array__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.array.is-array */ "../../node_modules/core-js/modules/es.array.is-array.js");
/* harmony import */ var core_js_modules_es_array_is_array__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_is_array__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ "../../node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_array_map__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.array.map */ "../../node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var core_js_modules_es_array_map__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_map__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.array.slice */ "../../node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.date.to-string */ "../../node_modules/core-js/modules/es.date.to-string.js");
/* harmony import */ var core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_es_function_bind__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/es.function.bind */ "../../node_modules/core-js/modules/es.function.bind.js");
/* harmony import */ var core_js_modules_es_function_bind__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_bind__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! core-js/modules/es.function.name */ "../../node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! core-js/modules/es.object.to-string */ "../../node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! core-js/modules/es.regexp.exec */ "../../node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! core-js/modules/es.regexp.to-string */ "../../node_modules/core-js/modules/es.regexp.to-string.js");
/* harmony import */ var core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_to_string__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! core-js/modules/es.string.iterator */ "../../node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! core-js/modules/es.string.replace */ "../../node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "../../node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "../../node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_19__);
/* harmony import */ var core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! core-js/modules/web.timers */ "../../node_modules/core-js/modules/web.timers.js");
/* harmony import */ var core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_20___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_20__);
/* harmony import */ var _dispatcher__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./dispatcher */ "./modules/dispatcher.js");
/* harmony import */ var _utils_documentReady__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../utils/documentReady */ "./utils/documentReady.js");






















function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }



var preloader = {
  el: null,
  images: [],
  backgroundEls: [],
  imagesNumber: 0,
  imagesLoaded: 0,
  transition: 1000,
  init: function init() {
    var _this = this;

    this.el = document.querySelector('#site-preloader');
    this.images = _toConsumableArray(document.images);
    this.backgroundEls = _toConsumableArray(document.querySelectorAll('.js-preloader-bg'));
    var imagesPaths = this.images.map(function (image) {
      return image.src;
    });
    var backgroundPaths = this.backgroundEls.map(function (elem) {
      var _window$getComputedSt = window.getComputedStyle(elem, false),
          backgroundImage = _window$getComputedSt.backgroundImage;

      return backgroundImage.slice(4, -1).replace(/"/g, '');
    });
    var allPaths = [].concat(_toConsumableArray(imagesPaths), _toConsumableArray(backgroundPaths)); // eslint-disable-next-line prefer-destructuring

    this.imagesNumber = allPaths.length;

    if (this.imagesNumber) {
      allPaths.forEach(function (imagesPath) {
        var clone = new Image();
        clone.addEventListener('load', _this.imageLoaded.bind(_this));
        clone.addEventListener('error', _this.imageLoaded.bind(_this));
        clone.src = imagesPath;
      });
    } else {
      this.preloaderHide();
    }
  },
  preloaderHide: function preloaderHide() {
    var transition = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.transition;
    var preloader = this.el;
    if (!preloader) return;
    _dispatcher__WEBPACK_IMPORTED_MODULE_21__["default"].dispatch({
      type: 'site-preloader:hiding'
    });
    preloader.style.transition = "opacity ".concat(transition, "ms ease, visibility ").concat(transition, "ms ease");
    preloader.classList.add('_loaded');
    document.body.classList.add('_site-loaded');
    setTimeout(function () {
      _dispatcher__WEBPACK_IMPORTED_MODULE_21__["default"].dispatch({
        type: 'site-preloader:removed'
      });
      preloader.remove();
      document.body.classList.add('_site-preloader-hidden');
    }, transition);
  },
  imageLoaded: function imageLoaded() {
    this.imagesLoaded += 1;

    if (this.imagesLoaded >= this.imagesNumber) {
      this.preloaderHide();
    }
  }
};
Object(_utils_documentReady__WEBPACK_IMPORTED_MODULE_22__["default"])(function () {
  preloader.init();
});
/* harmony default export */ __webpack_exports__["default"] = (preloader);

/***/ }),

/***/ "./polyfills.js":
/*!**********************!*\
  !*** ./polyfills.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_features_promise__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/features/promise */ "../../node_modules/core-js/features/promise/index.js");
/* harmony import */ var core_js_features_promise__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_features_promise__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_features_number_is_nan__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/features/number/is-nan */ "../../node_modules/core-js/features/number/is-nan.js");
/* harmony import */ var core_js_features_number_is_nan__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_features_number_is_nan__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_features_array_find__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/features/array/find */ "../../node_modules/core-js/features/array/find.js");
/* harmony import */ var core_js_features_array_find__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_features_array_find__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_features_string_starts_with__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/features/string/starts-with */ "../../node_modules/core-js/features/string/starts-with.js");
/* harmony import */ var core_js_features_string_starts_with__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_features_string_starts_with__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vendor_polyfills_js_requestAnimationFrame__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vendor/_polyfills/js/requestAnimationFrame */ "../vendor/_polyfills/js/requestAnimationFrame.js");
/* harmony import */ var vendor_polyfills_js_DOM_Element_matches__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vendor/_polyfills/js/DOM/Element.matches */ "../vendor/_polyfills/js/DOM/Element.matches.js");
/* harmony import */ var vendor_polyfills_js_DOM_Element_closest__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vendor/_polyfills/js/DOM/Element.closest */ "../vendor/_polyfills/js/DOM/Element.closest.js");
/* harmony import */ var vendor_polyfills_js_DOM_Element_closest__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(vendor_polyfills_js_DOM_Element_closest__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var vendor_polyfills_js_DOM_Node_remove__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! vendor/_polyfills/js/DOM/Node.remove */ "../vendor/_polyfills/js/DOM/Node.remove.js");
/* harmony import */ var vendor_polyfills_js_DOM_Node_after__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! vendor/_polyfills/js/DOM/Node.after */ "../vendor/_polyfills/js/DOM/Node.after.js");
/* harmony import */ var vendor_polyfills_js_DOM_Node_before__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! vendor/_polyfills/js/DOM/Node.before */ "../vendor/_polyfills/js/DOM/Node.before.js");
/* harmony import */ var vendor_polyfills_js_DOM_Node_replaceWith__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! vendor/_polyfills/js/DOM/Node.replaceWith */ "../vendor/_polyfills/js/DOM/Node.replaceWith.js");
/* harmony import */ var vendor_polyfills_js_DOM_ParentNode_append__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! vendor/_polyfills/js/DOM/ParentNode.append */ "../vendor/_polyfills/js/DOM/ParentNode.append.js");
/* harmony import */ var vendor_polyfills_js_DOM_ParentNode_prepend__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! vendor/_polyfills/js/DOM/ParentNode.prepend */ "../vendor/_polyfills/js/DOM/ParentNode.prepend.js");
// JS




 // DOM


 // зависимость - Element.matches








if (!SVGElement.prototype.contains) {
  SVGElement.prototype.contains = HTMLDivElement.prototype.contains;
} // customElements
// import 'vendor/_polyfills/customElements/document-register-element.max';

/***/ }),

/***/ "./utils/EventEmitter.js":
/*!*******************************!*\
  !*** ./utils/EventEmitter.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return EventEmitter; });
/* harmony import */ var core_js_modules_es_array_filter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.filter */ "../../node_modules/core-js/modules/es.array.filter.js");
/* harmony import */ var core_js_modules_es_array_filter__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_filter__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.for-each */ "../../node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_array_index_of__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.array.index-of */ "../../node_modules/core-js/modules/es.array.index-of.js");
/* harmony import */ var core_js_modules_es_array_index_of__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_index_of__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_object_define_property__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.object.define-property */ "../../node_modules/core-js/modules/es.object.define-property.js");
/* harmony import */ var core_js_modules_es_object_define_property__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_define_property__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.regexp.exec */ "../../node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_string_split__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.string.split */ "../../node_modules/core-js/modules/es.string.split.js");
/* harmony import */ var core_js_modules_es_string_split__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_split__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "../../node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each__WEBPACK_IMPORTED_MODULE_6__);








function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/* eslint-disable no-param-reassign */
var EventEmitter = /*#__PURE__*/function () {
  function EventEmitter() {
    _classCallCheck(this, EventEmitter);

    this._handlers = {
      all: []
    };
    this._frozen = false;
  }

  _createClass(EventEmitter, [{
    key: "dispatch",
    value: function dispatch(channel, event) {
      if (!event) {
        event = channel;
        channel = 'all';
      }

      if (event && event.type.indexOf(':')) {
        channel = event.type.split(':')[0];
      }

      if (!Object.prototype.hasOwnProperty.call(this._handlers, channel)) {
        this._handlers[channel] = [];
      }

      this._frozen = true;

      this._handlers[channel].forEach(function (handler) {
        return handler(event);
      });

      if (channel !== 'all') {
        this._handlers.all.forEach(function (handler) {
          return handler(event);
        });
      }

      this._frozen = false;
    }
  }, {
    key: "subscribe",
    value: function subscribe(channel, handler) {
      if (!handler) {
        handler = channel;
        channel = 'all';
      }

      if (this._frozen) {
        console.error('trying to subscribe to EventEmitter while dispatch is working');
      }

      if (typeof handler !== 'function') {
        console.error('handler has to be a function');
        return;
      }

      if (!Object.prototype.hasOwnProperty.call(this._handlers, channel)) {
        this._handlers[channel] = [];
      }

      if (this._handlers[channel].indexOf(handler) === -1) {
        this._handlers[channel].push(handler);
      } else {
        console.error('handler already set');
      }
    }
  }, {
    key: "unsubscribe",
    value: function unsubscribe(channel, handler) {
      if (!handler) {
        handler = channel;
        channel = 'all';
      }

      if (this._frozen) {
        console.error('trying to unsubscribe from EventEmitter while dispatch is working');
      }

      if (typeof handler !== 'function') {
        console.error('handler has to be a function');
      }

      if (!this._handlers[channel]) {
        console.error("channel ".concat(channel, " does not exist"));
        return;
      }

      if (this._handlers[channel].indexOf(handler) === -1) {
        console.log(handler);
        console.error('trying to unsubscribe unexisting handler');
        return;
      }

      this._handlers[channel] = this._handlers[channel].filter(function (h) {
        return h !== handler;
      });
    }
  }]);

  return EventEmitter;
}();



/***/ }),

/***/ "./utils/documentLoaded.js":
/*!*********************************!*\
  !*** ./utils/documentLoaded.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return documentLoaded; });
/**
 * Wait until document is loaded to run method
 * @param  {Function} fn Method to run
 */
function documentLoaded(fn) {
  // Sanity check
  if (typeof fn !== 'function') return; // If document is already loaded, run method

  if (document.readyState === 'complete') {
    return fn();
  } // Otherwise, wait until document is loaded


  window.addEventListener('load', fn, false);
}

/***/ }),

/***/ "./utils/documentReady.js":
/*!********************************!*\
  !*** ./utils/documentReady.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return documentReady; });
/**
 * Wait until document is ready to run method
 * @param  {Function} fn Method to run
 */
function documentReady(fn) {
  // Sanity check
  if (typeof fn !== 'function') return; // If document is already loaded, run method

  if (document.readyState === 'interactive' || document.readyState === 'complete') {
    return fn();
  } // Otherwise, wait until document is loaded


  document.addEventListener('DOMContentLoaded', fn, false);
}

/***/ }),

/***/ "./utils/getScrollWidth.js":
/*!*********************************!*\
  !*** ./utils/getScrollWidth.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return getScrollWidth; });
/* harmony import */ var core_js_modules_es_object_assign__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.assign */ "../../node_modules/core-js/modules/es.object.assign.js");
/* harmony import */ var core_js_modules_es_object_assign__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_assign__WEBPACK_IMPORTED_MODULE_0__);

function getScrollWidth() {
  var element = document.createElement('div');
  Object.assign(element.style, {
    overflowY: 'scroll',
    height: '50px',
    width: '50px',
    visibility: 'hidden'
  });
  document.body.append(element);
  var scrollWidth = element.offsetWidth - element.clientWidth;
  element.remove();
  return scrollWidth;
}

/***/ }),

/***/ "./utils/isTouchDevice.js":
/*!********************************!*\
  !*** ./utils/isTouchDevice.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return isTouchDevice; });
function isTouchDevice() {
  return 'ontouchstart' in window || navigator.maxTouchPoints;
}

/***/ }),

/***/ "./utils/throttle.js":
/*!***************************!*\
  !*** ./utils/throttle.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_date_now__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.date.now */ "../../node_modules/core-js/modules/es.date.now.js");
/* harmony import */ var core_js_modules_es_date_now__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_date_now__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.date.to-string */ "../../node_modules/core-js/modules/es.date.to-string.js");
/* harmony import */ var core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_date_to_string__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/web.timers */ "../../node_modules/core-js/modules/web.timers.js");
/* harmony import */ var core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_timers__WEBPACK_IMPORTED_MODULE_2__);




/* eslint-disable no-undefined,no-param-reassign,no-shadow */

/**
 * Throttle execution of a function. Especially useful for rate limiting
 * execution of handlers on events like resize and scroll.
 *
 * @param  {Number}    delay
 * A zero-or-greater delay in milliseconds.
 * For event callbacks, values around 100 or 250 (or even higher) are most useful.
 * @param  {Boolean}   [noTrailing]   Optional, defaults to false.
 * If noTrailing is true, callback will only execute every `delay` milliseconds while the
 * throttled-function is being called. If noTrailing is false or unspecified, callback will be executed one final time
 * after the last throttled-function call. (After the throttled-function has not been called for `delay` milliseconds,
 * the internal counter is reset)
 * @param  {Function}  callback
 * A function to be executed after delay milliseconds. The `this` context and all arguments are passed through, as-is,
 * to `callback` when the throttled-function is executed.
 * @param  {Boolean}   [debounceMode]
 * If `debounceMode` is true (at begin), schedule `clear` to execute after `delay` ms.
 * If `debounceMode` is false (at end), schedule `callback` to execute after `delay` ms.
 *
 * @return {Function}  A new, throttled, function.
 */
/* harmony default export */ __webpack_exports__["default"] = (function (delay, noTrailing, callback, debounceMode) {
  /*
   * After wrapper has stopped being called, this timeout ensures that
   * `callback` is executed at the proper times in `throttle` and `end`
   * debounce modes.
   */
  var timeoutID;
  var cancelled = false; // Keep track of the last time `callback` was executed.

  var lastExec = 0; // Function to clear existing timeout

  function clearExistingTimeout() {
    if (timeoutID) {
      clearTimeout(timeoutID);
    }
  } // Function to cancel next exec


  function cancel() {
    clearExistingTimeout();
    cancelled = true;
  } // `noTrailing` defaults to falsy.


  if (typeof noTrailing !== 'boolean') {
    debounceMode = callback;
    callback = noTrailing;
    noTrailing = undefined;
  }
  /*
   * The `wrapper` function encapsulates all of the throttling / debouncing
   * functionality and when executed will limit the rate at which `callback`
   * is executed.
   */


  function wrapper() {
    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    var self = this;
    var elapsed = Date.now() - lastExec;

    if (cancelled) {
      return;
    } // Execute `callback` and update the `lastExec` timestamp.


    function exec() {
      lastExec = Date.now();
      callback.apply(self, args);
    }
    /*
     * If `debounceMode` is true (at begin) this is used to clear the flag
     * to allow future `callback` executions.
     */


    function clear() {
      timeoutID = undefined;
    }

    if (debounceMode && !timeoutID) {
      /*
       * Since `wrapper` is being called for the first time and
       * `debounceMode` is true (at begin), execute `callback`.
       */
      exec();
    }

    clearExistingTimeout();

    if (debounceMode === undefined && elapsed > delay) {
      /*
       * In throttle mode, if `delay` time has been exceeded, execute
       * `callback`.
       */
      exec();
    } else if (noTrailing !== true) {
      /*
       * In trailing throttle mode, since `delay` time has not been
       * exceeded, schedule `callback` to execute `delay` ms after most
       * recent execution.
       *
       * If `debounceMode` is true (at begin), schedule `clear` to execute
       * after `delay` ms.
       *
       * If `debounceMode` is false (at end), schedule `callback` to
       * execute after `delay` ms.
       */
      timeoutID = setTimeout(debounceMode ? clear : exec, debounceMode === undefined ? delay - elapsed : delay);
    }
  }

  wrapper.cancel = cancel; // Return the wrapper function.

  return wrapper;
});

/***/ }),

/***/ 0:
/*!*******************************!*\
  !*** multi ./polyfills ./app ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./polyfills */"./polyfills.js");
module.exports = __webpack_require__(/*! ./app */"./app.js");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmJ1bmRsZS5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly8vLi9hcHAuanMiLCJ3ZWJwYWNrOi8vLy4vbW9kdWxlcy9jc3NWYXJzL2luZGV4LmpzIiwid2VicGFjazovLy8uL21vZHVsZXMvY3NzVmFycy9zY3JvbGxXaWR0aC5qcyIsIndlYnBhY2s6Ly8vLi9tb2R1bGVzL2Nzc1ZhcnMvdmguanMiLCJ3ZWJwYWNrOi8vLy4vbW9kdWxlcy9kaXNwYXRjaGVyLmpzIiwid2VicGFjazovLy8uL21vZHVsZXMvbGF6eWxvYWQuanMiLCJ3ZWJwYWNrOi8vLy4vbW9kdWxlcy9vdXJ3b3Jrc0xhYmVsLmpzIiwid2VicGFjazovLy8uL21vZHVsZXMvcmVzaXplLmpzIiwid2VicGFjazovLy8uL21vZHVsZXMvc2VhcmNoLmpzIiwid2VicGFjazovLy8uL21vZHVsZXMvc2l0ZVByZWxvYWRlci5qcyIsIndlYnBhY2s6Ly8vLi9wb2x5ZmlsbHMuanMiLCJ3ZWJwYWNrOi8vLy4vdXRpbHMvRXZlbnRFbWl0dGVyLmpzIiwid2VicGFjazovLy8uL3V0aWxzL2RvY3VtZW50TG9hZGVkLmpzIiwid2VicGFjazovLy8uL3V0aWxzL2RvY3VtZW50UmVhZHkuanMiLCJ3ZWJwYWNrOi8vLy4vdXRpbHMvZ2V0U2Nyb2xsV2lkdGguanMiLCJ3ZWJwYWNrOi8vLy4vdXRpbHMvaXNUb3VjaERldmljZS5qcyIsIndlYnBhY2s6Ly8vLi91dGlscy90aHJvdHRsZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBpbnN0YWxsIGEgSlNPTlAgY2FsbGJhY2sgZm9yIGNodW5rIGxvYWRpbmdcbiBcdGZ1bmN0aW9uIHdlYnBhY2tKc29ucENhbGxiYWNrKGRhdGEpIHtcbiBcdFx0dmFyIGNodW5rSWRzID0gZGF0YVswXTtcbiBcdFx0dmFyIG1vcmVNb2R1bGVzID0gZGF0YVsxXTtcbiBcdFx0dmFyIGV4ZWN1dGVNb2R1bGVzID0gZGF0YVsyXTtcblxuIFx0XHQvLyBhZGQgXCJtb3JlTW9kdWxlc1wiIHRvIHRoZSBtb2R1bGVzIG9iamVjdCxcbiBcdFx0Ly8gdGhlbiBmbGFnIGFsbCBcImNodW5rSWRzXCIgYXMgbG9hZGVkIGFuZCBmaXJlIGNhbGxiYWNrXG4gXHRcdHZhciBtb2R1bGVJZCwgY2h1bmtJZCwgaSA9IDAsIHJlc29sdmVzID0gW107XG4gXHRcdGZvcig7aSA8IGNodW5rSWRzLmxlbmd0aDsgaSsrKSB7XG4gXHRcdFx0Y2h1bmtJZCA9IGNodW5rSWRzW2ldO1xuIFx0XHRcdGlmKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChpbnN0YWxsZWRDaHVua3MsIGNodW5rSWQpICYmIGluc3RhbGxlZENodW5rc1tjaHVua0lkXSkge1xuIFx0XHRcdFx0cmVzb2x2ZXMucHVzaChpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF1bMF0pO1xuIFx0XHRcdH1cbiBcdFx0XHRpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0gPSAwO1xuIFx0XHR9XG4gXHRcdGZvcihtb2R1bGVJZCBpbiBtb3JlTW9kdWxlcykge1xuIFx0XHRcdGlmKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChtb3JlTW9kdWxlcywgbW9kdWxlSWQpKSB7XG4gXHRcdFx0XHRtb2R1bGVzW21vZHVsZUlkXSA9IG1vcmVNb2R1bGVzW21vZHVsZUlkXTtcbiBcdFx0XHR9XG4gXHRcdH1cbiBcdFx0aWYocGFyZW50SnNvbnBGdW5jdGlvbikgcGFyZW50SnNvbnBGdW5jdGlvbihkYXRhKTtcblxuIFx0XHR3aGlsZShyZXNvbHZlcy5sZW5ndGgpIHtcbiBcdFx0XHRyZXNvbHZlcy5zaGlmdCgpKCk7XG4gXHRcdH1cblxuIFx0XHQvLyBhZGQgZW50cnkgbW9kdWxlcyBmcm9tIGxvYWRlZCBjaHVuayB0byBkZWZlcnJlZCBsaXN0XG4gXHRcdGRlZmVycmVkTW9kdWxlcy5wdXNoLmFwcGx5KGRlZmVycmVkTW9kdWxlcywgZXhlY3V0ZU1vZHVsZXMgfHwgW10pO1xuXG4gXHRcdC8vIHJ1biBkZWZlcnJlZCBtb2R1bGVzIHdoZW4gYWxsIGNodW5rcyByZWFkeVxuIFx0XHRyZXR1cm4gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKTtcbiBcdH07XG4gXHRmdW5jdGlvbiBjaGVja0RlZmVycmVkTW9kdWxlcygpIHtcbiBcdFx0dmFyIHJlc3VsdDtcbiBcdFx0Zm9yKHZhciBpID0gMDsgaSA8IGRlZmVycmVkTW9kdWxlcy5sZW5ndGg7IGkrKykge1xuIFx0XHRcdHZhciBkZWZlcnJlZE1vZHVsZSA9IGRlZmVycmVkTW9kdWxlc1tpXTtcbiBcdFx0XHR2YXIgZnVsZmlsbGVkID0gdHJ1ZTtcbiBcdFx0XHRmb3IodmFyIGogPSAxOyBqIDwgZGVmZXJyZWRNb2R1bGUubGVuZ3RoOyBqKyspIHtcbiBcdFx0XHRcdHZhciBkZXBJZCA9IGRlZmVycmVkTW9kdWxlW2pdO1xuIFx0XHRcdFx0aWYoaW5zdGFsbGVkQ2h1bmtzW2RlcElkXSAhPT0gMCkgZnVsZmlsbGVkID0gZmFsc2U7XG4gXHRcdFx0fVxuIFx0XHRcdGlmKGZ1bGZpbGxlZCkge1xuIFx0XHRcdFx0ZGVmZXJyZWRNb2R1bGVzLnNwbGljZShpLS0sIDEpO1xuIFx0XHRcdFx0cmVzdWx0ID0gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBkZWZlcnJlZE1vZHVsZVswXSk7XG4gXHRcdFx0fVxuIFx0XHR9XG5cbiBcdFx0cmV0dXJuIHJlc3VsdDtcbiBcdH1cblxuIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gb2JqZWN0IHRvIHN0b3JlIGxvYWRlZCBhbmQgbG9hZGluZyBjaHVua3NcbiBcdC8vIHVuZGVmaW5lZCA9IGNodW5rIG5vdCBsb2FkZWQsIG51bGwgPSBjaHVuayBwcmVsb2FkZWQvcHJlZmV0Y2hlZFxuIFx0Ly8gUHJvbWlzZSA9IGNodW5rIGxvYWRpbmcsIDAgPSBjaHVuayBsb2FkZWRcbiBcdHZhciBpbnN0YWxsZWRDaHVua3MgPSB7XG4gXHRcdFwiYXBwXCI6IDBcbiBcdH07XG5cbiBcdHZhciBkZWZlcnJlZE1vZHVsZXMgPSBbXTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJqcy9cIjtcblxuIFx0dmFyIGpzb25wQXJyYXkgPSB3aW5kb3dbXCJ3ZWJwYWNrSnNvbnBcIl0gPSB3aW5kb3dbXCJ3ZWJwYWNrSnNvbnBcIl0gfHwgW107XG4gXHR2YXIgb2xkSnNvbnBGdW5jdGlvbiA9IGpzb25wQXJyYXkucHVzaC5iaW5kKGpzb25wQXJyYXkpO1xuIFx0anNvbnBBcnJheS5wdXNoID0gd2VicGFja0pzb25wQ2FsbGJhY2s7XG4gXHRqc29ucEFycmF5ID0ganNvbnBBcnJheS5zbGljZSgpO1xuIFx0Zm9yKHZhciBpID0gMDsgaSA8IGpzb25wQXJyYXkubGVuZ3RoOyBpKyspIHdlYnBhY2tKc29ucENhbGxiYWNrKGpzb25wQXJyYXlbaV0pO1xuIFx0dmFyIHBhcmVudEpzb25wRnVuY3Rpb24gPSBvbGRKc29ucEZ1bmN0aW9uO1xuXG5cbiBcdC8vIGFkZCBlbnRyeSBtb2R1bGUgdG8gZGVmZXJyZWQgbGlzdFxuIFx0ZGVmZXJyZWRNb2R1bGVzLnB1c2goWzAsXCJ2ZW5kb3JzXCJdKTtcbiBcdC8vIHJ1biBkZWZlcnJlZCBtb2R1bGVzIHdoZW4gcmVhZHlcbiBcdHJldHVybiBjaGVja0RlZmVycmVkTW9kdWxlcygpO1xuIiwiaW1wb3J0ICcuL21vZHVsZXMvc2l0ZVByZWxvYWRlcic7XG5pbXBvcnQgZG9jdW1lbnRSZWFkeSBmcm9tICcuL3V0aWxzL2RvY3VtZW50UmVhZHknO1xuaW1wb3J0IGRvY3VtZW50TG9hZGVkIGZyb20gJy4vdXRpbHMvZG9jdW1lbnRMb2FkZWQnO1xuXG5pbXBvcnQgY3NzVmFycyBmcm9tICcuL21vZHVsZXMvY3NzVmFycyc7XG5pbXBvcnQgcmVzaXplIGZyb20gJy4vbW9kdWxlcy9yZXNpemUnO1xuaW1wb3J0IGxhenlsb2FkIGZyb20gJy4vbW9kdWxlcy9sYXp5bG9hZCc7XG5pbXBvcnQgc2VhcmNoIGZyb20gJy4vbW9kdWxlcy9zZWFyY2gnO1xuaW1wb3J0IG91cndvcmtzTGFiZWwgZnJvbSAnLi9tb2R1bGVzL291cndvcmtzTGFiZWwnO1xuXG5cblxuZG9jdW1lbnRSZWFkeSgoKSA9PiB7XG4gIGNzc1ZhcnMuaW5pdCgpO1xuICByZXNpemUuaW5pdCgpO1xuICBsYXp5bG9hZC5pbml0KCk7XG59KTtcblxuZG9jdW1lbnRMb2FkZWQoKCkgPT4ge1xuXG59KTtcbiIsImltcG9ydCB2aCBmcm9tICcuL3ZoJztcbmltcG9ydCBzY3JvbGxXaWR0aCBmcm9tICcuL3Njcm9sbFdpZHRoJztcblxuZXhwb3J0IGRlZmF1bHQge1xuICBpbml0KCkge1xuICAgIHZoLmluaXQoKTtcbiAgICBzY3JvbGxXaWR0aC5pbml0KCk7XG4gIH0sXG59O1xuIiwiaW1wb3J0IGdldFNjcm9sbFdpZHRoIGZyb20gJy4uLy4uL3V0aWxzL2dldFNjcm9sbFdpZHRoJztcblxuZXhwb3J0IGRlZmF1bHQge1xuICBpbml0KCkge1xuICAgIHRoaXMuY2FsY3VsYXRlKCk7XG4gIH0sXG4gIGNhbGN1bGF0ZSgpIHtcbiAgICBjb25zdCBzY3JvbGxXaWR0aCA9IGdldFNjcm9sbFdpZHRoKCk7XG4gICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnN0eWxlLnNldFByb3BlcnR5KCctLXNjcm9sbC13aWR0aCcsIGAkeyBzY3JvbGxXaWR0aCB9cHhgKTtcbiAgfSxcbn07XG4iLCJpbXBvcnQgZGlzcGF0Y2hlciBmcm9tICcuLi9kaXNwYXRjaGVyJztcbmltcG9ydCBpc1RvdWNoRGV2aWNlIGZyb20gJy4uLy4uL3V0aWxzL2lzVG91Y2hEZXZpY2UnO1xuXG5leHBvcnQgZGVmYXVsdCB7XG4gIGluaXQoKSB7XG4gICAgdGhpcy5jYWxjdWxhdGUoKTtcbiAgICB0aGlzLmhhbmRsZVJlc2l6ZSgpO1xuICB9LFxuICBjYWxjdWxhdGUoKSB7XG4gICAgY29uc3QgdmggPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50SGVpZ2h0ICogMC4wMTtcbiAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc3R5bGUuc2V0UHJvcGVydHkoJy0tdmgnLCBgJHsgdmggfXB4YCk7XG4gIH0sXG4gIGhhbmRsZVJlc2l6ZSgpIHtcbiAgICBkaXNwYXRjaGVyLnN1YnNjcmliZSgoeyB0eXBlIH0pID0+IHtcbiAgICAgIGNvbnN0IHRhcmdldFR5cGUgPSBpc1RvdWNoRGV2aWNlKCkgPyAncmVzaXplOmJvdGgnIDogJ3Jlc2l6ZTpoZWlnaHQnO1xuXG4gICAgICBpZiAodHlwZSA9PT0gdGFyZ2V0VHlwZSkge1xuICAgICAgICB0aGlzLmNhbGN1bGF0ZSgpO1xuICAgICAgfVxuICAgIH0pO1xuICB9LFxufTtcbiIsImltcG9ydCBFdmVudEVtaXR0ZXIgZnJvbSAnLi4vdXRpbHMvRXZlbnRFbWl0dGVyJztcblxuZXhwb3J0IGRlZmF1bHQgbmV3IEV2ZW50RW1pdHRlcigpO1xuIiwiaW1wb3J0ICdjb3JlLWpzL2ZlYXR1cmVzL29iamVjdC9hc3NpZ24nO1xuaW1wb3J0ICdpbnRlcnNlY3Rpb24tb2JzZXJ2ZXInOyAvLyBwb2x5ZmlsbFxuaW1wb3J0IGxvemFkIGZyb20gJ2xvemFkJztcblxuZXhwb3J0IGRlZmF1bHQge1xuICBpbml0KHJvb3QgPSBkb2N1bWVudCkge1xuICAgIGNvbnN0IG9wdGlvbnMgPSB7XG4gICAgICByb290TWFyZ2luOiBgJHtkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50SGVpZ2h0fXB4IDBweGAsXG4gICAgfTtcblxuICAgIGNvbnN0IHBpY3R1cmVzID0gcm9vdC5xdWVyeVNlbGVjdG9yQWxsKCcuanMtbGF6eS1pbWc6bm90KFtkYXRhLWxvYWRlZF0pJyk7XG4gICAgY29uc3QgYmFja2dyb3VuZHMgPSByb290LnF1ZXJ5U2VsZWN0b3JBbGwoJy5qcy1sYXp5LWJnOm5vdChbZGF0YS1sb2FkZWRdKScpO1xuXG4gICAgaWYgKHBpY3R1cmVzLmxlbmd0aCkge1xuICAgICAgY29uc3QgcGljdHVyZU9ic2VydmVyID0gbG96YWQocGljdHVyZXMsIG9wdGlvbnMpO1xuICAgICAgcGljdHVyZU9ic2VydmVyLm9ic2VydmUoKTtcbiAgICB9XG5cbiAgICBpZiAoYmFja2dyb3VuZHMubGVuZ3RoKSB7XG4gICAgICBjb25zdCBiYWNrZ3JvdW5kT2JzZXJ2ZXIgPSBsb3phZChiYWNrZ3JvdW5kcywgb3B0aW9ucyk7XG4gICAgICBiYWNrZ3JvdW5kT2JzZXJ2ZXIub2JzZXJ2ZSgpO1xuICAgIH1cblxuICAgIHRoaXMubGF6eVZpZGVvKHJvb3QpO1xuICB9LFxuXG4gIGxhenlWaWRlbyhyb290ID0gZG9jdW1lbnQpIHtcbiAgICBjb25zdCBsYXp5VmlkZW9zID0gWy4uLnJvb3QucXVlcnlTZWxlY3RvckFsbCgndmlkZW8uanMtbGF6eS12aWRlbzpub3QoW2RhdGEtbG9hZGVkXSknKV07XG4gICAgY29uc3Qgb2JzZXJ2ZXJPcHRpb25zID0ge1xuICAgICAgcm9vdE1hcmdpbjogYCR7ZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsaWVudEhlaWdodH1weCAwcHhgLFxuICAgIH07XG5cbiAgICBpZiAoJ0ludGVyc2VjdGlvbk9ic2VydmVyJyBpbiB3aW5kb3cpIHtcbiAgICAgIGNvbnN0IGxhenlWaWRlb09ic2VydmVyID0gbmV3IEludGVyc2VjdGlvbk9ic2VydmVyKChlbnRyaWVzKSA9PiB7XG4gICAgICAgIGVudHJpZXMuZm9yRWFjaCgodmlkZW8pID0+IHtcbiAgICAgICAgICBpZiAodmlkZW8uaXNJbnRlcnNlY3RpbmcpIHtcbiAgICAgICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1yZXN0cmljdGVkLXN5bnRheCxndWFyZC1mb3ItaW5cbiAgICAgICAgICAgIGZvciAoY29uc3Qgc291cmNlIGluIHZpZGVvLnRhcmdldC5jaGlsZHJlbikge1xuICAgICAgICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgcHJlZmVyLWRlc3RydWN0dXJpbmdcbiAgICAgICAgICAgICAgY29uc3QgdmlkZW9Tb3VyY2UgPSB2aWRlby50YXJnZXQuY2hpbGRyZW5bc291cmNlXTtcbiAgICAgICAgICAgICAgaWYgKHR5cGVvZiB2aWRlb1NvdXJjZS50YWdOYW1lID09PSAnc3RyaW5nJyAmJiB2aWRlb1NvdXJjZS50YWdOYW1lID09PSAnU09VUkNFJykge1xuICAgICAgICAgICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBwcmVmZXItZGVzdHJ1Y3R1cmluZ1xuICAgICAgICAgICAgICAgIHZpZGVvU291cmNlLnNyYyA9IHZpZGVvU291cmNlLmRhdGFzZXQuc3JjO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHZpZGVvLnRhcmdldC5sb2FkKCk7XG4gICAgICAgICAgICB2aWRlby50YXJnZXQuc2V0QXR0cmlidXRlKCdkYXRhLWxvYWRlZCcsICd0cnVlJyk7XG4gICAgICAgICAgICBsYXp5VmlkZW9PYnNlcnZlci51bm9ic2VydmUodmlkZW8udGFyZ2V0KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfSwgb2JzZXJ2ZXJPcHRpb25zKTtcblxuICAgICAgbGF6eVZpZGVvcy5mb3JFYWNoKChsYXp5VmlkZW8pID0+IHtcbiAgICAgICAgbGF6eVZpZGVvT2JzZXJ2ZXIub2JzZXJ2ZShsYXp5VmlkZW8pO1xuICAgICAgfSk7XG4gICAgfVxuICB9LFxufTtcbiIsImZ1bmN0aW9uIGxhYmVsKCkge1xuICBsZXQgc3BhbkxhYmVscyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5zLW91cndvcmtzX2xhYmVsJyk7XG5cbiAgc3BhbkxhYmVscy5mb3JFYWNoKFxuICAgIGZ1bmN0aW9uIGdldE1hdGNoKGluZm8pIHtcbiAgICAgIGxldCBuYW1lTGFiZWxzID0gaW5mby5xdWVyeVNlbGVjdG9yKCcucy1vdXJ3b3Jrc19faXRlbS1pbWctbGFiZWwnKTtcbiAgICAgIGxldCBuYW1lTGFiZWxzSW5uZXIgPSBuYW1lTGFiZWxzLmlubmVySFRNTDtcblxuICAgICAgaWYgKG5hbWVMYWJlbHNJbm5lciA9PT0gJ0luZGVwZW5kZW50TGl2aW5nJykge1xuICAgICAgICBpbmZvLmNsYXNzTGlzdC5hZGQoJ3Mtb3Vyd29ya3NfbGFiZWwtZGFya0F6dXJlJyk7XG4gICAgICB9XG4gICAgICBlbHNlIGlmIChuYW1lTGFiZWxzSW5uZXIgPT09ICdTdXBwb3J0QXZhaWxhYmxlJykge1xuICAgICAgICBpbmZvLmNsYXNzTGlzdC5hZGQoJ3Mtb3Vyd29ya3NfbGFiZWwtb3JhbmdlJyk7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgaW5mby5jbGFzc0xpc3QuYWRkKCdzLW91cndvcmtzX2xhYmVsLW90aGVyZScpO1xuICAgICAgfVxuICAgIH1cbiAgKVxufVxubGFiZWwoKTtcblxuIiwiaW1wb3J0IGRpc3BhdGNoZXIgZnJvbSAnLi9kaXNwYXRjaGVyJztcbmltcG9ydCB0aHJvdHRsZSBmcm9tICcuLi91dGlscy90aHJvdHRsZSc7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgc2l6ZToge1xuICAgIHdpZHRoOiB3aW5kb3cuaW5uZXJXaWR0aCxcbiAgICBoZWlnaHQ6IHdpbmRvdy5pbm5lckhlaWdodCxcbiAgfSxcbiAgaW5pdCgpIHtcbiAgICBjb25zdCBzZWxmID0gdGhpcztcblxuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLCB0aHJvdHRsZSgzMDAsIGZhbHNlLCAoKSA9PiB7XG4gICAgICBzZWxmLmhhbmRsZVJlc2l6ZSgpO1xuICAgIH0pLCBmYWxzZSk7XG5cbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignb3JpZW50YXRpb25jaGFuZ2UnLCB0aHJvdHRsZSgzMDAsIGZhbHNlLCAoKSA9PiB7XG4gICAgICBzZWxmLmhhbmRsZVJlc2l6ZSgpO1xuICAgIH0pLCBmYWxzZSk7XG4gIH0sXG4gIGhhbmRsZVJlc2l6ZSgpIHtcbiAgICAvKiBlc2xpbnQtZGlzYWJsZSBwcmVmZXItZGVzdHJ1Y3R1cmluZyAqL1xuICAgIGNvbnN0IHdpZHRoID0gd2luZG93LmlubmVyV2lkdGg7XG4gICAgY29uc3QgaGVpZ2h0ID0gd2luZG93LmlubmVySGVpZ2h0O1xuICAgIC8qIGVzbGludC1lbmFibGUgcHJlZmVyLWRlc3RydWN0dXJpbmcgKi9cblxuICAgIGNvbnN0IHdpZHRoQ2hhbmdlZCA9IHdpZHRoICE9PSB0aGlzLnNpemUud2lkdGg7XG4gICAgY29uc3QgaGVpZ2h0Q2hhbmdlZCA9IGhlaWdodCAhPT0gdGhpcy5zaXplLmhlaWdodDtcblxuICAgIGlmICh3aWR0aENoYW5nZWQpIHtcbiAgICAgIGRpc3BhdGNoZXIuZGlzcGF0Y2goe1xuICAgICAgICB0eXBlOiAncmVzaXplOndpZHRoJyxcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGlmIChoZWlnaHRDaGFuZ2VkKSB7XG4gICAgICBkaXNwYXRjaGVyLmRpc3BhdGNoKHtcbiAgICAgICAgdHlwZTogJ3Jlc2l6ZTpoZWlnaHQnLFxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgaWYgKHdpZHRoQ2hhbmdlZCAmJiBoZWlnaHRDaGFuZ2VkKSB7XG4gICAgICBkaXNwYXRjaGVyLmRpc3BhdGNoKHtcbiAgICAgICAgdHlwZTogJ3Jlc2l6ZTpib3RoJyxcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGRpc3BhdGNoZXIuZGlzcGF0Y2goe1xuICAgICAgdHlwZTogJ3Jlc2l6ZTpkZWZhdWx0JyxcbiAgICB9KTtcblxuICAgIHRoaXMuc2l6ZSA9IHtcbiAgICAgIHdpZHRoLFxuICAgICAgaGVpZ2h0LFxuICAgIH07XG4gIH0sXG59O1xuIiwiXG5mdW5jdGlvbiBmaWx0ZXIoZXZ0KSB7XG4gIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xuICBsZXQgaW5wdXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjcy1vdXJ3b3Jrc19fZmlsdGVycy1pbnB1dCcpO1xuICBsZXQgaW5wdXRWYWx1ZSA9IGlucHV0LnZhbHVlLnRvVXBwZXJDYXNlKCk7XG4gIGxldCBjYXJkcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5zLW91cndvcmtzX19pdGVtJyk7XG5cbiAgaWYgKGlucHV0VmFsdWUubGVuZ3RoID4gMykge1xuICAgIGNhcmRzLmZvckVhY2goXG4gICAgICBmdW5jdGlvbiBnZXRNYXRjaChpbmZvKSB7XG4gICAgICAgIGxldCBoZWFkaW5nID0gaW5mby5xdWVyeVNlbGVjdG9yKCdoNCcpO1xuICAgICAgICBsZXQgaGVhZGluZ0NvbnRlbnQgPSBoZWFkaW5nLmlubmVySFRNTC50b1VwcGVyQ2FzZSgpO1xuXG4gICAgICAgIGlmIChoZWFkaW5nQ29udGVudC5pbmNsdWRlcyhpbnB1dFZhbHVlKSkge1xuICAgICAgICAgIGluZm8uY2xhc3NMaXN0LmFkZCgnc2hvdycpO1xuICAgICAgICAgIGluZm8uY2xhc3NMaXN0LnJlbW92ZSgnaGlkZScpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIGluZm8uY2xhc3NMaXN0LmFkZCgnaGlkZScpO1xuICAgICAgICAgIGluZm8uY2xhc3NMaXN0LnJlbW92ZSgnc2hvdycpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgKVxuICB9IGVsc2Uge1xuICAgIGNhcmRzLmZvckVhY2goXG4gICAgICBmdW5jdGlvbiBnZXRNYXRjaChpbmZvKSB7XG4gICAgICAgIGxldCBoZWFkaW5nID0gaW5mby5xdWVyeVNlbGVjdG9yKCdoNCcpO1xuICAgICAgICBsZXQgaGVhZGluZ0NvbnRlbnQgPSBoZWFkaW5nLmlubmVySFRNTC50b1VwcGVyQ2FzZSgpO1xuXG4gICAgICAgIGlmIChoZWFkaW5nQ29udGVudC5pbmNsdWRlcyhpbnB1dFZhbHVlKSkge1xuICAgICAgICAgIGluZm8uY2xhc3NMaXN0LnJlbW92ZSgnaGlkZScpO1xuICAgICAgICAgIGluZm8uY2xhc3NMaXN0LmFkZCgnc2hvdycpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIGluZm8uY2xhc3NMaXN0LnJlbW92ZSgnaGlkZScpO1xuICAgICAgICAgIGluZm8uY2xhc3NMaXN0LmFkZCgnc2hvdycpO1xuICAgICAgICB9XG4gICAgICB9KVxuICB9XG59XG5cblxubGV0IGZvcm0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcucy1vdXJ3b3Jrc19fZmlsdGVycy1mb3JtJyk7XG5mb3JtLmFkZEV2ZW50TGlzdGVuZXIoJ2tleXVwJywgZmlsdGVyKTtcbiIsImltcG9ydCBkaXNwYXRjaGVyIGZyb20gJy4vZGlzcGF0Y2hlcic7XG5pbXBvcnQgZG9jdW1lbnRSZWFkeSBmcm9tICcuLi91dGlscy9kb2N1bWVudFJlYWR5JztcblxuY29uc3QgcHJlbG9hZGVyID0ge1xuICBlbDogbnVsbCxcbiAgaW1hZ2VzOiBbXSxcbiAgYmFja2dyb3VuZEVsczogW10sXG4gIGltYWdlc051bWJlcjogMCxcbiAgaW1hZ2VzTG9hZGVkOiAwLFxuICB0cmFuc2l0aW9uOiAxMDAwLFxuXG4gIGluaXQoKSB7XG4gICAgdGhpcy5lbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNzaXRlLXByZWxvYWRlcicpO1xuICAgIHRoaXMuaW1hZ2VzID0gWy4uLmRvY3VtZW50LmltYWdlc107XG4gICAgdGhpcy5iYWNrZ3JvdW5kRWxzID0gWy4uLmRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5qcy1wcmVsb2FkZXItYmcnKV07XG5cbiAgICBjb25zdCBpbWFnZXNQYXRocyA9IHRoaXMuaW1hZ2VzLm1hcCgoaW1hZ2UpID0+IGltYWdlLnNyYyk7XG4gICAgY29uc3QgYmFja2dyb3VuZFBhdGhzID0gdGhpcy5iYWNrZ3JvdW5kRWxzLm1hcCgoZWxlbSkgPT4ge1xuICAgICAgY29uc3QgeyBiYWNrZ3JvdW5kSW1hZ2UgfSA9IHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGVsZW0sIGZhbHNlKTtcbiAgICAgIHJldHVybiBiYWNrZ3JvdW5kSW1hZ2Uuc2xpY2UoNCwgLTEpLnJlcGxhY2UoL1wiL2csICcnKTtcbiAgICB9KTtcbiAgICBjb25zdCBhbGxQYXRocyA9IFsuLi5pbWFnZXNQYXRocywgLi4uYmFja2dyb3VuZFBhdGhzXTtcblxuICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBwcmVmZXItZGVzdHJ1Y3R1cmluZ1xuICAgIHRoaXMuaW1hZ2VzTnVtYmVyID0gYWxsUGF0aHMubGVuZ3RoO1xuXG4gICAgaWYgKHRoaXMuaW1hZ2VzTnVtYmVyKSB7XG4gICAgICBhbGxQYXRocy5mb3JFYWNoKChpbWFnZXNQYXRoKSA9PiB7XG4gICAgICAgIGNvbnN0IGNsb25lID0gbmV3IEltYWdlKCk7XG4gICAgICAgIGNsb25lLmFkZEV2ZW50TGlzdGVuZXIoJ2xvYWQnLCB0aGlzLmltYWdlTG9hZGVkLmJpbmQodGhpcykpO1xuICAgICAgICBjbG9uZS5hZGRFdmVudExpc3RlbmVyKCdlcnJvcicsIHRoaXMuaW1hZ2VMb2FkZWQuYmluZCh0aGlzKSk7XG4gICAgICAgIGNsb25lLnNyYyA9IGltYWdlc1BhdGg7XG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5wcmVsb2FkZXJIaWRlKCk7XG4gICAgfVxuICB9LFxuXG4gIHByZWxvYWRlckhpZGUodHJhbnNpdGlvbiA9IHRoaXMudHJhbnNpdGlvbikge1xuICAgIGNvbnN0IHsgZWw6IHByZWxvYWRlciB9ID0gdGhpcztcbiAgICBpZiAoIXByZWxvYWRlcikgcmV0dXJuO1xuXG4gICAgZGlzcGF0Y2hlci5kaXNwYXRjaCh7XG4gICAgICB0eXBlOiAnc2l0ZS1wcmVsb2FkZXI6aGlkaW5nJyxcbiAgICB9KTtcblxuICAgIHByZWxvYWRlci5zdHlsZS50cmFuc2l0aW9uID0gYG9wYWNpdHkgJHsgdHJhbnNpdGlvbiB9bXMgZWFzZSwgdmlzaWJpbGl0eSAkeyB0cmFuc2l0aW9uIH1tcyBlYXNlYDtcbiAgICBwcmVsb2FkZXIuY2xhc3NMaXN0LmFkZCgnX2xvYWRlZCcpO1xuICAgIGRvY3VtZW50LmJvZHkuY2xhc3NMaXN0LmFkZCgnX3NpdGUtbG9hZGVkJyk7XG5cbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgIGRpc3BhdGNoZXIuZGlzcGF0Y2goe1xuICAgICAgICB0eXBlOiAnc2l0ZS1wcmVsb2FkZXI6cmVtb3ZlZCcsXG4gICAgICB9KTtcblxuICAgICAgcHJlbG9hZGVyLnJlbW92ZSgpO1xuICAgICAgZG9jdW1lbnQuYm9keS5jbGFzc0xpc3QuYWRkKCdfc2l0ZS1wcmVsb2FkZXItaGlkZGVuJyk7XG4gICAgfSwgdHJhbnNpdGlvbik7XG4gIH0sXG5cbiAgaW1hZ2VMb2FkZWQoKSB7XG4gICAgdGhpcy5pbWFnZXNMb2FkZWQgKz0gMTtcblxuICAgIGlmICh0aGlzLmltYWdlc0xvYWRlZCA+PSB0aGlzLmltYWdlc051bWJlcikge1xuICAgICAgdGhpcy5wcmVsb2FkZXJIaWRlKCk7XG4gICAgfVxuICB9LFxufTtcblxuZG9jdW1lbnRSZWFkeSgoKSA9PiB7XG4gIHByZWxvYWRlci5pbml0KCk7XG59KTtcblxuZXhwb3J0IGRlZmF1bHQgcHJlbG9hZGVyO1xuIiwiLy8gSlNcbmltcG9ydCAnY29yZS1qcy9mZWF0dXJlcy9wcm9taXNlJztcbmltcG9ydCAnY29yZS1qcy9mZWF0dXJlcy9udW1iZXIvaXMtbmFuJztcbmltcG9ydCAnY29yZS1qcy9mZWF0dXJlcy9hcnJheS9maW5kJztcbmltcG9ydCAnY29yZS1qcy9mZWF0dXJlcy9zdHJpbmcvc3RhcnRzLXdpdGgnO1xuaW1wb3J0ICd2ZW5kb3IvX3BvbHlmaWxscy9qcy9yZXF1ZXN0QW5pbWF0aW9uRnJhbWUnO1xuXG4vLyBET01cbmltcG9ydCAndmVuZG9yL19wb2x5ZmlsbHMvanMvRE9NL0VsZW1lbnQubWF0Y2hlcyc7XG5pbXBvcnQgJ3ZlbmRvci9fcG9seWZpbGxzL2pzL0RPTS9FbGVtZW50LmNsb3Nlc3QnOyAvLyDQt9Cw0LLQuNGB0LjQvNC+0YHRgtGMIC0gRWxlbWVudC5tYXRjaGVzXG5pbXBvcnQgJ3ZlbmRvci9fcG9seWZpbGxzL2pzL0RPTS9Ob2RlLnJlbW92ZSc7XG5pbXBvcnQgJ3ZlbmRvci9fcG9seWZpbGxzL2pzL0RPTS9Ob2RlLmFmdGVyJztcbmltcG9ydCAndmVuZG9yL19wb2x5ZmlsbHMvanMvRE9NL05vZGUuYmVmb3JlJztcbmltcG9ydCAndmVuZG9yL19wb2x5ZmlsbHMvanMvRE9NL05vZGUucmVwbGFjZVdpdGgnO1xuaW1wb3J0ICd2ZW5kb3IvX3BvbHlmaWxscy9qcy9ET00vUGFyZW50Tm9kZS5hcHBlbmQnO1xuaW1wb3J0ICd2ZW5kb3IvX3BvbHlmaWxscy9qcy9ET00vUGFyZW50Tm9kZS5wcmVwZW5kJztcblxuaWYgKCFTVkdFbGVtZW50LnByb3RvdHlwZS5jb250YWlucykge1xuICBTVkdFbGVtZW50LnByb3RvdHlwZS5jb250YWlucyA9IEhUTUxEaXZFbGVtZW50LnByb3RvdHlwZS5jb250YWlucztcbn1cblxuLy8gY3VzdG9tRWxlbWVudHNcbi8vIGltcG9ydCAndmVuZG9yL19wb2x5ZmlsbHMvY3VzdG9tRWxlbWVudHMvZG9jdW1lbnQtcmVnaXN0ZXItZWxlbWVudC5tYXgnO1xuIiwiLyogZXNsaW50LWRpc2FibGUgbm8tcGFyYW0tcmVhc3NpZ24gKi9cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEV2ZW50RW1pdHRlciB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMuX2hhbmRsZXJzID0ge1xuICAgICAgYWxsOiBbXSxcbiAgICB9O1xuICAgIHRoaXMuX2Zyb3plbiA9IGZhbHNlO1xuICB9XG5cbiAgZGlzcGF0Y2goY2hhbm5lbCwgZXZlbnQpIHtcbiAgICBpZiAoIWV2ZW50KSB7XG4gICAgICBldmVudCA9IGNoYW5uZWw7XG4gICAgICBjaGFubmVsID0gJ2FsbCc7XG4gICAgfVxuXG4gICAgaWYgKGV2ZW50ICYmIGV2ZW50LnR5cGUuaW5kZXhPZignOicpKSB7XG4gICAgICBjaGFubmVsID0gZXZlbnQudHlwZS5zcGxpdCgnOicpWzBdO1xuICAgIH1cblxuICAgIGlmICghT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHRoaXMuX2hhbmRsZXJzLCBjaGFubmVsKSkge1xuICAgICAgdGhpcy5faGFuZGxlcnNbY2hhbm5lbF0gPSBbXTtcbiAgICB9XG5cbiAgICB0aGlzLl9mcm96ZW4gPSB0cnVlO1xuXG4gICAgdGhpcy5faGFuZGxlcnNbY2hhbm5lbF0uZm9yRWFjaCgoaGFuZGxlcikgPT4gaGFuZGxlcihldmVudCkpO1xuXG4gICAgaWYgKGNoYW5uZWwgIT09ICdhbGwnKSB7XG4gICAgICB0aGlzLl9oYW5kbGVycy5hbGwuZm9yRWFjaCgoaGFuZGxlcikgPT4gaGFuZGxlcihldmVudCkpO1xuICAgIH1cblxuICAgIHRoaXMuX2Zyb3plbiA9IGZhbHNlO1xuICB9XG5cbiAgc3Vic2NyaWJlKGNoYW5uZWwsIGhhbmRsZXIpIHtcbiAgICBpZiAoIWhhbmRsZXIpIHtcbiAgICAgIGhhbmRsZXIgPSBjaGFubmVsO1xuICAgICAgY2hhbm5lbCA9ICdhbGwnO1xuICAgIH1cblxuICAgIGlmICh0aGlzLl9mcm96ZW4pIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoJ3RyeWluZyB0byBzdWJzY3JpYmUgdG8gRXZlbnRFbWl0dGVyIHdoaWxlIGRpc3BhdGNoIGlzIHdvcmtpbmcnKTtcbiAgICB9XG5cbiAgICBpZiAodHlwZW9mIGhhbmRsZXIgIT09ICdmdW5jdGlvbicpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoJ2hhbmRsZXIgaGFzIHRvIGJlIGEgZnVuY3Rpb24nKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAoIU9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbCh0aGlzLl9oYW5kbGVycywgY2hhbm5lbCkpIHtcbiAgICAgIHRoaXMuX2hhbmRsZXJzW2NoYW5uZWxdID0gW107XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuX2hhbmRsZXJzW2NoYW5uZWxdLmluZGV4T2YoaGFuZGxlcikgPT09IC0xKSB7XG4gICAgICB0aGlzLl9oYW5kbGVyc1tjaGFubmVsXS5wdXNoKGhhbmRsZXIpO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb25zb2xlLmVycm9yKCdoYW5kbGVyIGFscmVhZHkgc2V0Jyk7XG4gICAgfVxuICB9XG5cbiAgdW5zdWJzY3JpYmUoY2hhbm5lbCwgaGFuZGxlcikge1xuICAgIGlmICghaGFuZGxlcikge1xuICAgICAgaGFuZGxlciA9IGNoYW5uZWw7XG4gICAgICBjaGFubmVsID0gJ2FsbCc7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuX2Zyb3plbikge1xuICAgICAgY29uc29sZS5lcnJvcigndHJ5aW5nIHRvIHVuc3Vic2NyaWJlIGZyb20gRXZlbnRFbWl0dGVyIHdoaWxlIGRpc3BhdGNoIGlzIHdvcmtpbmcnKTtcbiAgICB9XG5cbiAgICBpZiAodHlwZW9mIGhhbmRsZXIgIT09ICdmdW5jdGlvbicpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoJ2hhbmRsZXIgaGFzIHRvIGJlIGEgZnVuY3Rpb24nKTtcbiAgICB9XG5cbiAgICBpZiAoIXRoaXMuX2hhbmRsZXJzW2NoYW5uZWxdKSB7XG4gICAgICBjb25zb2xlLmVycm9yKGBjaGFubmVsICR7IGNoYW5uZWwgfSBkb2VzIG5vdCBleGlzdGApO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmICh0aGlzLl9oYW5kbGVyc1tjaGFubmVsXS5pbmRleE9mKGhhbmRsZXIpID09PSAtMSkge1xuICAgICAgY29uc29sZS5sb2coaGFuZGxlcik7XG4gICAgICBjb25zb2xlLmVycm9yKCd0cnlpbmcgdG8gdW5zdWJzY3JpYmUgdW5leGlzdGluZyBoYW5kbGVyJyk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5faGFuZGxlcnNbY2hhbm5lbF0gPSB0aGlzLl9oYW5kbGVyc1tjaGFubmVsXS5maWx0ZXIoKGgpID0+IGggIT09IGhhbmRsZXIpO1xuICB9XG59XG4iLCIvKipcbiAqIFdhaXQgdW50aWwgZG9jdW1lbnQgaXMgbG9hZGVkIHRvIHJ1biBtZXRob2RcbiAqIEBwYXJhbSAge0Z1bmN0aW9ufSBmbiBNZXRob2QgdG8gcnVuXG4gKi9cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGRvY3VtZW50TG9hZGVkKGZuKSB7XG4gIC8vIFNhbml0eSBjaGVja1xuICBpZiAodHlwZW9mIGZuICE9PSAnZnVuY3Rpb24nKSByZXR1cm47XG5cbiAgLy8gSWYgZG9jdW1lbnQgaXMgYWxyZWFkeSBsb2FkZWQsIHJ1biBtZXRob2RcbiAgaWYgKGRvY3VtZW50LnJlYWR5U3RhdGUgPT09ICdjb21wbGV0ZScpIHtcbiAgICByZXR1cm4gZm4oKTtcbiAgfVxuXG4gIC8vIE90aGVyd2lzZSwgd2FpdCB1bnRpbCBkb2N1bWVudCBpcyBsb2FkZWRcbiAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ2xvYWQnLCBmbiwgZmFsc2UpO1xufVxuIiwiLyoqXG4gKiBXYWl0IHVudGlsIGRvY3VtZW50IGlzIHJlYWR5IHRvIHJ1biBtZXRob2RcbiAqIEBwYXJhbSAge0Z1bmN0aW9ufSBmbiBNZXRob2QgdG8gcnVuXG4gKi9cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGRvY3VtZW50UmVhZHkoZm4pIHtcbiAgLy8gU2FuaXR5IGNoZWNrXG4gIGlmICh0eXBlb2YgZm4gIT09ICdmdW5jdGlvbicpIHJldHVybjtcblxuICAvLyBJZiBkb2N1bWVudCBpcyBhbHJlYWR5IGxvYWRlZCwgcnVuIG1ldGhvZFxuICBpZiAoZG9jdW1lbnQucmVhZHlTdGF0ZSA9PT0gJ2ludGVyYWN0aXZlJyB8fCBkb2N1bWVudC5yZWFkeVN0YXRlID09PSAnY29tcGxldGUnKSB7XG4gICAgcmV0dXJuIGZuKCk7XG4gIH1cblxuICAvLyBPdGhlcndpc2UsIHdhaXQgdW50aWwgZG9jdW1lbnQgaXMgbG9hZGVkXG4gIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCBmbiwgZmFsc2UpO1xufVxuIiwiZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gZ2V0U2Nyb2xsV2lkdGgoKSB7XG4gIGNvbnN0IGVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgT2JqZWN0LmFzc2lnbihlbGVtZW50LnN0eWxlLCB7XG4gICAgb3ZlcmZsb3dZOiAnc2Nyb2xsJyxcbiAgICBoZWlnaHQ6ICc1MHB4JyxcbiAgICB3aWR0aDogJzUwcHgnLFxuICAgIHZpc2liaWxpdHk6ICdoaWRkZW4nLFxuICB9KTtcbiAgZG9jdW1lbnQuYm9keS5hcHBlbmQoZWxlbWVudCk7XG4gIGNvbnN0IHNjcm9sbFdpZHRoID0gZWxlbWVudC5vZmZzZXRXaWR0aCAtIGVsZW1lbnQuY2xpZW50V2lkdGg7XG4gIGVsZW1lbnQucmVtb3ZlKCk7XG5cbiAgcmV0dXJuIHNjcm9sbFdpZHRoO1xufVxuIiwiZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gaXNUb3VjaERldmljZSgpIHtcbiAgcmV0dXJuICdvbnRvdWNoc3RhcnQnIGluIHdpbmRvdyB8fCBuYXZpZ2F0b3IubWF4VG91Y2hQb2ludHM7XG59XG4iLCIvKiBlc2xpbnQtZGlzYWJsZSBuby11bmRlZmluZWQsbm8tcGFyYW0tcmVhc3NpZ24sbm8tc2hhZG93ICovXG5cbi8qKlxuICogVGhyb3R0bGUgZXhlY3V0aW9uIG9mIGEgZnVuY3Rpb24uIEVzcGVjaWFsbHkgdXNlZnVsIGZvciByYXRlIGxpbWl0aW5nXG4gKiBleGVjdXRpb24gb2YgaGFuZGxlcnMgb24gZXZlbnRzIGxpa2UgcmVzaXplIGFuZCBzY3JvbGwuXG4gKlxuICogQHBhcmFtICB7TnVtYmVyfSAgICBkZWxheVxuICogQSB6ZXJvLW9yLWdyZWF0ZXIgZGVsYXkgaW4gbWlsbGlzZWNvbmRzLlxuICogRm9yIGV2ZW50IGNhbGxiYWNrcywgdmFsdWVzIGFyb3VuZCAxMDAgb3IgMjUwIChvciBldmVuIGhpZ2hlcikgYXJlIG1vc3QgdXNlZnVsLlxuICogQHBhcmFtICB7Qm9vbGVhbn0gICBbbm9UcmFpbGluZ10gICBPcHRpb25hbCwgZGVmYXVsdHMgdG8gZmFsc2UuXG4gKiBJZiBub1RyYWlsaW5nIGlzIHRydWUsIGNhbGxiYWNrIHdpbGwgb25seSBleGVjdXRlIGV2ZXJ5IGBkZWxheWAgbWlsbGlzZWNvbmRzIHdoaWxlIHRoZVxuICogdGhyb3R0bGVkLWZ1bmN0aW9uIGlzIGJlaW5nIGNhbGxlZC4gSWYgbm9UcmFpbGluZyBpcyBmYWxzZSBvciB1bnNwZWNpZmllZCwgY2FsbGJhY2sgd2lsbCBiZSBleGVjdXRlZCBvbmUgZmluYWwgdGltZVxuICogYWZ0ZXIgdGhlIGxhc3QgdGhyb3R0bGVkLWZ1bmN0aW9uIGNhbGwuIChBZnRlciB0aGUgdGhyb3R0bGVkLWZ1bmN0aW9uIGhhcyBub3QgYmVlbiBjYWxsZWQgZm9yIGBkZWxheWAgbWlsbGlzZWNvbmRzLFxuICogdGhlIGludGVybmFsIGNvdW50ZXIgaXMgcmVzZXQpXG4gKiBAcGFyYW0gIHtGdW5jdGlvbn0gIGNhbGxiYWNrXG4gKiBBIGZ1bmN0aW9uIHRvIGJlIGV4ZWN1dGVkIGFmdGVyIGRlbGF5IG1pbGxpc2Vjb25kcy4gVGhlIGB0aGlzYCBjb250ZXh0IGFuZCBhbGwgYXJndW1lbnRzIGFyZSBwYXNzZWQgdGhyb3VnaCwgYXMtaXMsXG4gKiB0byBgY2FsbGJhY2tgIHdoZW4gdGhlIHRocm90dGxlZC1mdW5jdGlvbiBpcyBleGVjdXRlZC5cbiAqIEBwYXJhbSAge0Jvb2xlYW59ICAgW2RlYm91bmNlTW9kZV1cbiAqIElmIGBkZWJvdW5jZU1vZGVgIGlzIHRydWUgKGF0IGJlZ2luKSwgc2NoZWR1bGUgYGNsZWFyYCB0byBleGVjdXRlIGFmdGVyIGBkZWxheWAgbXMuXG4gKiBJZiBgZGVib3VuY2VNb2RlYCBpcyBmYWxzZSAoYXQgZW5kKSwgc2NoZWR1bGUgYGNhbGxiYWNrYCB0byBleGVjdXRlIGFmdGVyIGBkZWxheWAgbXMuXG4gKlxuICogQHJldHVybiB7RnVuY3Rpb259ICBBIG5ldywgdGhyb3R0bGVkLCBmdW5jdGlvbi5cbiAqL1xuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gKGRlbGF5LCBub1RyYWlsaW5nLCBjYWxsYmFjaywgZGVib3VuY2VNb2RlKSB7XG4gIC8qXG4gICAqIEFmdGVyIHdyYXBwZXIgaGFzIHN0b3BwZWQgYmVpbmcgY2FsbGVkLCB0aGlzIHRpbWVvdXQgZW5zdXJlcyB0aGF0XG4gICAqIGBjYWxsYmFja2AgaXMgZXhlY3V0ZWQgYXQgdGhlIHByb3BlciB0aW1lcyBpbiBgdGhyb3R0bGVgIGFuZCBgZW5kYFxuICAgKiBkZWJvdW5jZSBtb2Rlcy5cbiAgICovXG4gIGxldCB0aW1lb3V0SUQ7XG4gIGxldCBjYW5jZWxsZWQgPSBmYWxzZTtcblxuICAvLyBLZWVwIHRyYWNrIG9mIHRoZSBsYXN0IHRpbWUgYGNhbGxiYWNrYCB3YXMgZXhlY3V0ZWQuXG4gIGxldCBsYXN0RXhlYyA9IDA7XG5cbiAgLy8gRnVuY3Rpb24gdG8gY2xlYXIgZXhpc3RpbmcgdGltZW91dFxuICBmdW5jdGlvbiBjbGVhckV4aXN0aW5nVGltZW91dCgpIHtcbiAgICBpZiAodGltZW91dElEKSB7XG4gICAgICBjbGVhclRpbWVvdXQodGltZW91dElEKTtcbiAgICB9XG4gIH1cblxuICAvLyBGdW5jdGlvbiB0byBjYW5jZWwgbmV4dCBleGVjXG4gIGZ1bmN0aW9uIGNhbmNlbCgpIHtcbiAgICBjbGVhckV4aXN0aW5nVGltZW91dCgpO1xuICAgIGNhbmNlbGxlZCA9IHRydWU7XG4gIH1cblxuICAvLyBgbm9UcmFpbGluZ2AgZGVmYXVsdHMgdG8gZmFsc3kuXG4gIGlmICh0eXBlb2Ygbm9UcmFpbGluZyAhPT0gJ2Jvb2xlYW4nKSB7XG4gICAgZGVib3VuY2VNb2RlID0gY2FsbGJhY2s7XG4gICAgY2FsbGJhY2sgPSBub1RyYWlsaW5nO1xuICAgIG5vVHJhaWxpbmcgPSB1bmRlZmluZWQ7XG4gIH1cblxuICAvKlxuICAgKiBUaGUgYHdyYXBwZXJgIGZ1bmN0aW9uIGVuY2Fwc3VsYXRlcyBhbGwgb2YgdGhlIHRocm90dGxpbmcgLyBkZWJvdW5jaW5nXG4gICAqIGZ1bmN0aW9uYWxpdHkgYW5kIHdoZW4gZXhlY3V0ZWQgd2lsbCBsaW1pdCB0aGUgcmF0ZSBhdCB3aGljaCBgY2FsbGJhY2tgXG4gICAqIGlzIGV4ZWN1dGVkLlxuICAgKi9cbiAgZnVuY3Rpb24gd3JhcHBlciguLi5hcmdzKSB7XG4gICAgY29uc3Qgc2VsZiA9IHRoaXM7XG4gICAgY29uc3QgZWxhcHNlZCA9IERhdGUubm93KCkgLSBsYXN0RXhlYztcblxuICAgIGlmIChjYW5jZWxsZWQpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICAvLyBFeGVjdXRlIGBjYWxsYmFja2AgYW5kIHVwZGF0ZSB0aGUgYGxhc3RFeGVjYCB0aW1lc3RhbXAuXG4gICAgZnVuY3Rpb24gZXhlYygpIHtcbiAgICAgIGxhc3RFeGVjID0gRGF0ZS5ub3coKTtcbiAgICAgIGNhbGxiYWNrLmFwcGx5KHNlbGYsIGFyZ3MpO1xuICAgIH1cblxuICAgIC8qXG4gICAgICogSWYgYGRlYm91bmNlTW9kZWAgaXMgdHJ1ZSAoYXQgYmVnaW4pIHRoaXMgaXMgdXNlZCB0byBjbGVhciB0aGUgZmxhZ1xuICAgICAqIHRvIGFsbG93IGZ1dHVyZSBgY2FsbGJhY2tgIGV4ZWN1dGlvbnMuXG4gICAgICovXG4gICAgZnVuY3Rpb24gY2xlYXIoKSB7XG4gICAgICB0aW1lb3V0SUQgPSB1bmRlZmluZWQ7XG4gICAgfVxuXG4gICAgaWYgKGRlYm91bmNlTW9kZSAmJiAhdGltZW91dElEKSB7XG4gICAgICAvKlxuICAgICAgICogU2luY2UgYHdyYXBwZXJgIGlzIGJlaW5nIGNhbGxlZCBmb3IgdGhlIGZpcnN0IHRpbWUgYW5kXG4gICAgICAgKiBgZGVib3VuY2VNb2RlYCBpcyB0cnVlIChhdCBiZWdpbiksIGV4ZWN1dGUgYGNhbGxiYWNrYC5cbiAgICAgICAqL1xuICAgICAgZXhlYygpO1xuICAgIH1cblxuICAgIGNsZWFyRXhpc3RpbmdUaW1lb3V0KCk7XG5cbiAgICBpZiAoZGVib3VuY2VNb2RlID09PSB1bmRlZmluZWQgJiYgZWxhcHNlZCA+IGRlbGF5KSB7XG4gICAgICAvKlxuICAgICAgICogSW4gdGhyb3R0bGUgbW9kZSwgaWYgYGRlbGF5YCB0aW1lIGhhcyBiZWVuIGV4Y2VlZGVkLCBleGVjdXRlXG4gICAgICAgKiBgY2FsbGJhY2tgLlxuICAgICAgICovXG4gICAgICBleGVjKCk7XG4gICAgfSBlbHNlIGlmIChub1RyYWlsaW5nICE9PSB0cnVlKSB7XG4gICAgICAvKlxuICAgICAgICogSW4gdHJhaWxpbmcgdGhyb3R0bGUgbW9kZSwgc2luY2UgYGRlbGF5YCB0aW1lIGhhcyBub3QgYmVlblxuICAgICAgICogZXhjZWVkZWQsIHNjaGVkdWxlIGBjYWxsYmFja2AgdG8gZXhlY3V0ZSBgZGVsYXlgIG1zIGFmdGVyIG1vc3RcbiAgICAgICAqIHJlY2VudCBleGVjdXRpb24uXG4gICAgICAgKlxuICAgICAgICogSWYgYGRlYm91bmNlTW9kZWAgaXMgdHJ1ZSAoYXQgYmVnaW4pLCBzY2hlZHVsZSBgY2xlYXJgIHRvIGV4ZWN1dGVcbiAgICAgICAqIGFmdGVyIGBkZWxheWAgbXMuXG4gICAgICAgKlxuICAgICAgICogSWYgYGRlYm91bmNlTW9kZWAgaXMgZmFsc2UgKGF0IGVuZCksIHNjaGVkdWxlIGBjYWxsYmFja2AgdG9cbiAgICAgICAqIGV4ZWN1dGUgYWZ0ZXIgYGRlbGF5YCBtcy5cbiAgICAgICAqL1xuICAgICAgdGltZW91dElEID0gc2V0VGltZW91dChkZWJvdW5jZU1vZGUgPyBjbGVhciA6IGV4ZWMsIGRlYm91bmNlTW9kZSA9PT0gdW5kZWZpbmVkID8gZGVsYXkgLSBlbGFwc2VkIDogZGVsYXkpO1xuICAgIH1cbiAgfVxuXG4gIHdyYXBwZXIuY2FuY2VsID0gY2FuY2VsO1xuXG4gIC8vIFJldHVybiB0aGUgd3JhcHBlciBmdW5jdGlvbi5cbiAgcmV0dXJuIHdyYXBwZXI7XG59XG4iXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDdkpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7OztBQ2xCQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBOzs7Ozs7Ozs7Ozs7QUNIQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFqQkE7Ozs7Ozs7Ozs7OztBQ0hBO0FBQUE7QUFBQTtBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQXJEQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNKQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBOzs7Ozs7Ozs7Ozs7QUNwQkE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBbkRBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM0NBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUVBO0FBRUE7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBL0RBO0FBa0VBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7QUN6RUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBR0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3RCQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3RGQTtBQUFBO0FBQUE7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUNmQTtBQUFBO0FBQUE7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7OztBQ2ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFFQTtBQUNBOzs7Ozs7Ozs7Ozs7QUNiQTtBQUFBO0FBQUE7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBcUJBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7O0FBS0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7OztBIiwic291cmNlUm9vdCI6IiJ9