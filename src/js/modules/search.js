
function filter(evt) {
  evt.preventDefault();
  let input = document.querySelector('#s-ourworks__filters-input');
  let inputValue = input.value.toUpperCase();
  let cards = document.querySelectorAll('.s-ourworks__item');

  if (inputValue.length > 3) {
    cards.forEach(
      function getMatch(info) {
        let heading = info.querySelector('h4');
        let headingContent = heading.innerHTML.toUpperCase();

        if (headingContent.includes(inputValue)) {
          info.classList.add('show');
          info.classList.remove('hide');
        }
        else {
          info.classList.add('hide');
          info.classList.remove('show');
        }
      }
    )
  } else {
    cards.forEach(
      function getMatch(info) {
        let heading = info.querySelector('h4');
        let headingContent = heading.innerHTML.toUpperCase();

        if (headingContent.includes(inputValue)) {
          info.classList.remove('hide');
          info.classList.add('show');
        }
        else {
          info.classList.remove('hide');
          info.classList.add('show');
        }
      })
  }
}


let form = document.querySelector('.s-ourworks__filters-form');
form.addEventListener('keyup', filter);
